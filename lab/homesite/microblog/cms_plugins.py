# -*- coding: utf-8 -*-
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from microblog import models

class MicroblogPlugin(CMSPluginBase):
    model = models.Microblog
    name = 'Microblog'
    render_template = 'microblog/stub.html'

    def render(self, context, instance, placeholder):
        context.update({'instance': instance, 'name': self.name})
        return context

    def render(self, context, instance, placeholder):
        context.update({
            'latest_tweet_list': models.Article.objects.all().order_by('-pub_date'),
            'placeholder': placeholder
        })
        return context

plugin_pool.register_plugin(MicroblogPlugin)

# -*- coding: utf-8 -*-
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from fblike import models

class FacebookLikePlugin(CMSPluginBase):
    model = models.FbLike
    name = 'FbLike'
    render_template = 'like.html'

    def render(self, context, instance, placeholder):
        context.update({'instance': instance, 'name': self.name})
        return context

    def render(self, context, instance, placeholder):
        context.update({
            'like': instance,
            'placeholder': placeholder
        })
        return context

plugin_pool.register_plugin(FacebookLikePlugin)

# -*- coding: utf-8 -*-
from cms.models import CMSPlugin
from django.db import models
from django.utils.translation import ugettext_lazy as _

LAYOUT_CHOICES = [
    ('standard', _('standard')),
    ('box_count', _('box_count')),
    ('button_count', _('button_count')),
]

class FbLike(CMSPlugin):
    layout = models.CharField(_("Layout"), choices=LAYOUT_CHOICES, default='standard', max_length=50)
